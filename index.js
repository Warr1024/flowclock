'use strict';

const path = require('path');
const express = require('express');
const helmet = require('helmet');
const compression = require('compression');
const config = require('./config');

async function main() {
	const app = express();

	if(config.slow)
		app.use((req, res, next) => setTimeout(next, config.slow));

	app.use('/stm', express.static(path.join(__dirname, 'node_modules', '@fontsource', 'share-tech-mono')));
	app.use('/vue', express.static(path.join(__dirname, 'node_modules', 'vue', 'dist')));
	app.use('/', express.static(path.join(__dirname, 'static')));

	const outer = express();

	const self = `'self'`;
	const unsafeInline = `'unsafe-inline'`;
	const unsafeEval = `'unsafe-eval'`;
	outer.use(helmet({
		hsts: false, // Handle this at reverse proxy
		contentSecurityPolicy: {
			directives: {
				defaultSrc: [self],
				scriptSrc: [self, unsafeInline, unsafeEval],
				styleSrc: [self, unsafeInline]
			}
		},
		referrerPolicy: {
			policy: 'same-origin'
		}
	}));
	app.use(compression({ level: config.compress || 1 }));

	outer.use(config.mount, app);
	outer.listen(config.port, config.host, () => console.log('listening'));
}

main();
