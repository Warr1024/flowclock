'use strict';

const mystore = localStorage || sessionStorage || (() => {
	const rawstore = {};
	return { get: k => rawstore[k], set: (k, v) => rawstore[k] = v };
})();

const app = new Vue({
	el: '#app',
	data: {
		ready: !navigator.serviceWorker
	},
	template: `
	<div>
		<div v-if="!ready" id="pageloading">
			<img class="spin" src="flowclock-transparent.png?cachefirst" />
		</div>
		<h1 v-if="ready">FlowClock</h1>
	</div>
	`
});

let justLoaded = true;
setTimeout(() => justLoaded = false, 10000);

let reloadReady = true;
if(navigator.serviceWorker) {
	navigator.serviceWorker.addEventListener('message', evt => {
		if(evt.data === 'reload') {
			reloadReady = false;
			return justLoaded && window.location.reload();
		}
	});
	navigator.serviceWorker.register('service-worker.js');
	navigator.serviceWorker.ready.then(() => setTimeout(() => app.ready = reloadReady, 100));
}
