'use strict';

const log = msg => {
	console.log(`service-worker: ${msg}`);
};

self.addEventListener('fetch', evt => {
	const cache = caches.open('main');

	const rx = /\?cachefirst$/;
	const cachefirst = rx.test(evt.request.url);

	const req = cachefirst ? new Request(evt.request.url.replace(rx, ''), evt.request) : evt.request;
	const fetching = fetch(req);

	if(/\/api\//.test(req.url)) {
		log(`fetch: ${req.url} -> api`);
		return evt.respondWith(fetching);
	}

	const rpt = s => log(`fetch ${req.mode} ${req.url} -> ${s}`);
	evt.respondWith(cache
		.then(c => c.match(req))
		.then(found => {
			if(found && cachefirst) {
				log(`fetch: ${req.url} -> cached`);
				fetching.then(resp => {
						rpt('cached');
						cache.then(c => c.put(req, resp));
					})
					.catch(() => {});
				return found;
			}
			return new Promise((res, rej) => {
				let to = found && setTimeout(() => {
					rpt('timeout');
					if(to) clearTimeout(to);
					to = undefined;
					return res(found);
				}, 2000);
				const failed = err => {
					rpt(found ? 'fallback' : 'fail');
					if(to) clearTimeout(to);
					to = undefined;
					return found ? res(found) : rej(err);
				};
				fetching.then(resp => {
						if(!resp.ok)
							return failed(`HTTP ${resp.status}`);
						rpt(to ? 'fresh' : 'update');
						if(to) clearTimeout(to);
						to = undefined;
						cache.then(c => c.put(req, resp)
							.then(() => c.match(req))
							.then(res));
					})
					.catch(failed);
			});
		}));
});

self.addEventListener('install', () => {
	log('install');
	self.skipWaiting();
});

self.addEventListener('activate', function(evt) {
	log('activate');
	evt.waitUntil(self.clients.claim()
		.then(
			self.clients.matchAll()
			.then(cl => cl.forEach(c => {
				c.postMessage('reload');
			}))
		));
});
