'use strict';

const path = require('path');
const os = require('os');
const minimist = require('minimist');

module.exports = minimist(process.argv.slice(2), {
	default: {
		port: 8080,
		host: '0.0.0.0',
		mount: '/',
		tempdir: path.join(os.tmpdir(), 'flowclock-' + os.userInfo()
			.username)
	}
});

console.log(`config: ${JSON.stringify(module.exports)}`);
